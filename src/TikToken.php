<?php

namespace topthink\tiktoken;

class TikToken
{
    private const ENCODINGS = [
        'cl100k_base' => [
            'vocab' => 'cl100k_base.tiktoken',
            'pat'   => '/(?i:\'s|\'t|\'re|\'ve|\'m|\'ll|\'d)|[^\r\n\p{L}\p{N}]?\p{L}+|\p{N}{1,3}| ?[^\s\p{L}\p{N}]+[\r\n]*|\s*[\r\n]+|\s+(?!\S)|\s+/u',
        ],
    ];

    protected $vocabs = [];

    public function nums($messages)
    {
        $perMessage = 3;
        $perName    = 1;

        $encoder = $this->getEncoder('cl100k_base');

        if (is_string($messages)) {
            return count($encoder->encode($messages));
        }

        $nums = 0;

        foreach ($messages as $message) {
            $nums += $perMessage;
            foreach ($message as $key => $value) {
                if ($key == 'tool_calls') {
                    foreach ($value as $call) {
                        foreach ($call as $cKey => $cValue) {
                            $nums += count($encoder->encode($cKey));
                            if ($cKey == 'function') {
                                foreach ($cValue as $fKey => $fValue) {
                                    $nums += count($encoder->encode($fKey));
                                    $nums += count($encoder->encode($fValue));
                                }
                            } else {
                                $nums += count($encoder->encode($cValue));
                            }
                        }
                    }
                } else {
                    if (is_array($value)) {
                        $text = '';
                        foreach ($value as $v) {
                            if (is_array($v)) {
                                switch ($v['type']) {
                                    case 'text':
                                        $text .= $v['text'];
                                        break;
                                    case 'image_url':
                                        $detail = $v['image_url']['detail'] ?? 'high';
                                        $nums   += $detail == 'low' ? 85 : 1000;
                                        break;
                                }
                            }
                        }
                        $value = $text;
                    }
                    if (is_string($value)) {
                        $nums += count($encoder->encode($value));
                    }
                }

                if ($key == 'name') {
                    $nums += $perName;
                }
            }
        }

        $nums += 3;

        return $nums;
    }

    protected function getEncoder($name): Encoder
    {
        return new Encoder(
            $name,
            $this->getVocab($name),
            self::ENCODINGS[$name]['pat'],
        );
    }

    protected function getVocab(string $name): Vocab
    {
        if (isset($this->vocabs[$name])) {
            return $this->vocabs[$name];
        }

        return $this->vocabs[$name] = Vocab::fromUri(__DIR__ . '/encodings/' . self::ENCODINGS[$name]['vocab']);
    }

}
