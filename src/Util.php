<?php

declare(strict_types = 1);

namespace topthink\tiktoken;

use Closure;
use function array_map;
use function bin2hex;
use function pack;
use function str_split;

final class Util
{
    public static function toBytes(string $text): array
    {
        return array_map(Closure::fromCallable('hexdec'), str_split(bin2hex($text), 2));
    }

    public static function fromBytes(array $bytes): string
    {
        return pack('C*', ...$bytes);
    }
}
